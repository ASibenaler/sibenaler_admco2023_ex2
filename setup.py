from setuptools import setup

setup(
    name="PackageMorpion-ASibenaler",
    version="0.0.1",
    author="Arnaud Sibenaler",
    packages=["Package_Morpion"],
    description="Package permettant de simuler un jeu de morpion",
    license="GNU GPLv3",
    python_requires=">=3.4",
)

